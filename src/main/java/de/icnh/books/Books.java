package de.icnh.books;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Path("books")
public class Books {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Book> getBooks(@QueryParam("limit") int limit) {
        return Stream.iterate(1, i -> i + 1).map(i -> {
            Book book = new Book();
            book.setTitle("Java EE " + i);
            book.setAuthor("ich");
            return book;
        }).limit(limit == 0 ? 1 : limit).collect(Collectors.toList());
    }
}
