package de.icnh.books;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.net.URI;

public class Server {

    public static void main(String[] args) {
        URI uri = URI.create("http://localhost:9998/");
        ResourceConfig config = new ResourceConfig(Books.class);
        JdkHttpServerFactory.createHttpServer(uri, config);
    }
}
